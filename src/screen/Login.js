import { React, Component } from "react";
import { Link } from "react-router-dom";
class Login extends Component {
  render() {
    return (
      <>
        <div className="App div-1024">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-10">
              <div className="row ">
                <div className="col-6 box-middle">
                  <h1>Sign In</h1>
                  <div className="row">
                    <div className="col-4">
                      <i
                        class="fab fa-facebook-square"
                        style={{ fontSize: 36 }}
                      />
                    </div>
                    <div className="col-4">
                      <i
                        class="fab fa-google-plus-square"
                        style={{ fontSize: 36 }}
                      />
                    </div>
                    <div className="col-4">
                      <i class="fab fa-linkedin" style={{ fontSize: 36 }} />
                    </div>
                  </div>

                  <hr />
                  <span>or use your account</span>
                  <input
                    type="text"
                    className="box-input"
                    placeholder="Username"
                  />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Password"
                  />

                  <Link
                    to={"/Home"}
                    className="button"
                    style={{ textDecoration: "none" }}
                  >
                    Sign In
                </Link>
                </div>

                <div className="col-6 box-middle ">
                  <Link
                    to={"/Register"}
                    className="button"
                    style={{ textDecoration: "none" }}
                  >
                    Sign Up
                </Link>
                </div>
              </div>
            </div>

            <div className="col-1"></div>
          </div>
        </div>




        {/*768px*/}
        <div className="App div-768">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-10">
              <div className="row ">
                <div className="col-10 box-middle">
                  <h1>Sign In</h1>
                  <div className="row">
                    <div className="col-4">
                      <i
                        class="fab fa-facebook-square"
                        style={{ fontSize: 36 }}
                      />
                    </div>
                    <div className="col-4">
                      <i
                        class="fab fa-google-plus-square"
                        style={{ fontSize: 36 }}
                      />
                    </div>
                    <div className="col-4">
                      <i class="fab fa-linkedin" style={{ fontSize: 36 }} />
                    </div>
                  </div>

                  <hr />
                  <span>or use your account</span>
                  <input
                    type="text"
                    className="box-input"
                    placeholder="Username"
                  />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Password"
                  />

                  <Link
                    to={"/Home"}
                    className="button"
                    style={{ textDecoration: "none" }}
                  >
                    Sign In
                </Link>
                  <div>
                    <Link
                      to={"/Register"}
                      style={{ textDecoration: "none" }}
                    >
                      Sign Up
                </Link></div>
                </div>
              </div>
            </div>

            <div className="col-1"></div>
          </div>
        </div>


        {/*425px*/}
        <div className="App div-425">
          <div className="row ">
            <div className="col-10 box-middle">
              <h1>Sign In</h1>
              <div className="row">
                <div className="col-4">
                  <i
                    class="fab fa-facebook-square"
                    style={{ fontSize: 36 }}
                  />
                </div>
                <div className="col-4">
                  <i
                    class="fab fa-google-plus-square"
                    style={{ fontSize: 36 }}
                  />
                </div>
                <div className="col-4">
                  <i class="fab fa-linkedin" style={{ fontSize: 36 }} />
                </div>
              </div>

              <hr />
              <span>or use your account</span>
              <input
                type="text"
                className="box-input"
                placeholder="Username"
              />
              <input
                type="password"
                className="box-input"
                placeholder="Password"
              />

              <Link
                to={"/Home"}
                className="button"
                style={{ textDecoration: "none" }}
              >
                Sign In
                </Link>
              <div>
                <Link
                  to={"/Register"}
                  style={{ textDecoration: "none" }}
                >
                  Sign Up
                </Link>
                </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Login;
