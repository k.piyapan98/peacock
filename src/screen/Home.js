import { React, Component } from "react";
import { Link } from "react-router-dom";
import NavigationBar from "../component/NavigationBar";
import "../CSS/Home.css";
import f from "../img/1.jpg";
const Group_ = [
  {
    ID_group: 0,
    title_group: "Title1",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 1,
    title_group: "Title2",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 2,
    title_group: "Title3",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 3,
    title_group: "Title4",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 4,
    title_group: "Title5",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 5,
    title_group: "Title6",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 6,
    title_group: "Title7",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 7,
    title_group: "Title8",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 8,
    title_group: "Title9",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 9,
    title_group: "Title10",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 10,
    title_group: "Title11",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
  {
    ID_group: 11,
    title_group: "Title12",
    detial_group:
      "detaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetaildetail",
    img_group: f,
  },
];

class Home extends Component {
  render() {
    return (
      <>
        <NavigationBar />
        <div className="App div-1024px">
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              <div className="search">
                <input type="text" placeholder="Search" />
                <div className="lens-rotale">
                  <span className="lens">&#9740;</span>
                </div>
              </div>
            </div>
            <div className="col-1"></div>
          </div>
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              {/*header*/}
              <div className="row" style={{ marginBottom: 25 }}>
                <div className="col-2">
                  <div style={{ fontSize: "36px" }}>Group</div>
                </div>
                <div className="col-2">
                  <Link to={"/Group"} style={{ textDecoration: "none" }}>
                    <div className="arrowFromLeft">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-plus"
                        viewBox="0 0 16 16"
                      >
                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                      </svg>
                      <span>New Group</span>
                    </div>
                  </Link>
                </div>
              </div>
              {/*header*/}

              <div className="row">
                <div className="col-12">
                  {Group_.map((item_group, index_group) => {
                    return (
                      <div className="card-group">
                        <img src={item_group.img_group} />
                        <div style={{ fontSize: "24px" }}>
                          {item_group.title_group}
                        </div>
                        <div className="card-detail">
                          {item_group.detial_group}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="col-1"></div>
        </div>

        {/* 768px */}

        <div className="App div-768px">
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              <div className="search">
                <input type="text" placeholder="Search" />
                <div className="lens-rotale">
                  <span className="lens">&#9740;</span>
                </div>
              </div>
            </div>
            <div className="col-1"></div>
          </div>
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              {/*header*/}
              <div className="row" style={{ marginBottom: 25 }}>
                <div className="col-2">
                  <div style={{ fontSize: "36px" }}>Group</div>
                </div>
                <div className="col-2">
                  <div className="arrowFromLeft">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      class="bi bi-plus"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                    </svg>
                    <span>New Group</span>
                  </div>
                </div>
              </div>
              {/*header*/}

              <div className="row">
                <div className="col-12">
                  {Group_.map((item_group, index_group) => {
                    return (
                      <div className="card-group">
                        <img src={item_group.img_group} />
                        <div style={{ fontSize: "24px" }}>
                          {item_group.title_group}
                        </div>
                        <div className="card-detail">
                          {item_group.detial_group}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="col-1"></div>
        </div>

        {/* 425px */}

        <div className="App div-425px">
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              <div className="search">
                <input type="text" placeholder="Search" />
                <div className="lens-rotale">
                  <span className="lens">&#9740;</span>
                </div>
              </div>
            </div>
            <div className="col-1"></div>
          </div>
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              {/*header*/}
              <div className="row" style={{ marginBottom: 25 }}>
                <div className="col-2">
                  <div style={{ fontSize: "36px" }}>Group</div>
                </div>
                <div className="col-2">
                  <div className="arrowFromLeft">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      class="bi bi-plus"
                      viewBox="0 0 16 16"
                    >
                      <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                    </svg>
                    <span>New Group</span>
                  </div>
                </div>
              </div>
              {/*header*/}

              <div className="row">
                <div className="col-12">
                  {Group_.map((item_group, index_group) => {
                    return (
                      <div className="card-group">
                        <img src={item_group.img_group} />
                        <div style={{ fontSize: "24px" }}>
                          {item_group.title_group}
                        </div>
                        <div className="card-detail">
                          {item_group.detial_group}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div className="col-1"></div>
        </div>
      </>
    );
  }
}

export default Home;
