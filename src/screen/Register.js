import { React, Component } from "react";
import { Link } from "react-router-dom";

class Register extends Component {
  render() {
    return (
      <>
        <div className="App div-1024px">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-10">
              <div className="row ">
                <div className="col-6 box-middle">
                  <Link
                    to={"/"}
                    className="button"
                    style={{ textDecoration: "none" }}
                  >
                    Sign In
                </Link>
                </div>

                <div className="col-6 box-middle">
                  <h1>Sign Up</h1>
                  <input
                    type="text"
                    className="box-input"
                    placeholder="Username"
                  />
                  <input type="email" className="box-input" placeholder="Email" />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Password"
                  />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Comfrim Password"
                  />

                  <button className="button">Sign Up</button>
                </div>
              </div>
            </div>

            <div className="col-1"></div>
          </div>
        </div>


        {/* 768px */}
        <div className="App div-768px">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-10 ">
              <div className="row box-middle">
                <div className="col-6 box-middle">
                  <h1>Sign Up</h1>
                  <input
                    type="text"
                    className="box-input"
                    placeholder="Username"
                  />
                  <input type="email" className="box-input" placeholder="Email" />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Password"
                  />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Comfrim Password"
                  />
                  <button className="button">Sign Up</button>
                </div>
                Already have an account? 
                <Link
                  to={"/"}

                  style={{ textDecoration: "none" }}
                >
                  Sign In
                </Link>
              </div>
            </div>

            <div className="col-1"></div>
          </div>
        </div>

        {/* 425px */}
        <div className="App div-425px">
          <div className="row">
            <div className="col-1"></div>
            <div className="col-10">
              <div className="row box-middle">

               


                <div className="col-6 box-middle">
                  <h1>Sign Up</h1>
                  <input
                    type="text"
                    className="box-input"
                    placeholder="Username"
                  />
                  <input type="email" className="box-input" placeholder="Email" />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Password"
                  />
                  <input
                    type="password"
                    className="box-input"
                    placeholder="Comfrim Password"
                  />

                  <button className="button">Sign Up</button>
                </div>
                Already have an account? 
                <Link
                  to={"/"}
                  style={{ textDecoration: "none" }}
                >
                  Sign In
                </Link>
              </div>
            </div>

            <div className="col-1"></div>
          </div>
        </div>
      </>
    );
  }
}

export default Register;
