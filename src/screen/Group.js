import { React, Component } from "react";
import { Link } from "react-router-dom";
import NavigationBar from "../component/NavigationBar";
import "../CSS/Group.css";
class Group extends Component {
  render() {
    return (
      <>
        <NavigationBar />
        <div className="App">
          <div className="row" style={{ marginLeft: "50px" }}>
            <div className="col-1"></div>
            <div className="col-10">
              <Link
                to={"/Home"}
                className="btn-back"
                style={{ textDecoration: "none" }}
              >
                <svg width="16px" height="16px" viewBox="0 -3 20 20">
                  <polyline points="10 0 3 8 10 16"></polyline>
                </svg>
                {/* <span>Home</span> */}
              </Link>
            </div>
            <div className="col-1"></div>
          </div>

          <div
            className="row"
            style={{ marginLeft: "50px", marginTop: "50px" }}
          >
            <div className="col-1"></div>
            <div className="col-3">
              <div className="add-img">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="50"
                  height="50"
                  fill="currentColor"
                  class="bi bi-image"
                  viewBox="0 0 16 16"
                >
                  <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                  <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z" />
                </svg>
              </div>
              <input
                type="file"
                className="input-img"
                accept="image/x-png,image/gif,image/jpeg"
              />
            </div>
            <div className="col-7">
              Name Group
              <div style={{marginBottom:"20px"}}>
                <input
                  type="text"
                  className="nameGroup"
                  placeholder="Name Group"
                />
              </div>
              Detail
              <div>
                <textarea type="areatext" rows="4" cols="50" className="detailGroup" placeholder="Detail..."/>
              </div>
            </div>
            <div className="col-1"></div>
          </div>
        </div>
      </>
    );
  }
}

export default Group;
