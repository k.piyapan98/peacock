import { BrowserRouter as Router, Route } from "react-router-dom";
import { React, Component } from "react";
import "./App.css";
import Login from "./screen/Login";
import Register from "./screen/Register";
import Home from "./screen/Home";
import Group from "./screen/Group";

class App extends Component {
  render() {
    return (
      <Router exact path="/">
        <Route exact path="/" component={Login} />
        <Route exact path="/Register" component={Register} />

        <Route exact path="/Home" component={Home} />
        <Route exact path="/Group" component={Group} />
      </Router>
    );
  }
}

export default App;
