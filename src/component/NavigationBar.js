import { React, Component } from "react";
import { Link } from "react-router-dom";
import "../CSS/NavigationBar.css";
class NavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BarNavigate: false,
      dd: false,
    };
  }

  render() {
    return (
      <div className="App">
        <div className={this.state.BarNavigate ? "bar-250" : "bar-50"}>
          {this.state.BarNavigate ? (
            <>
              <span
                className="span-bar"
                onClick={() => {
                  this.setState({ BarNavigate: !this.state.BarNavigate });
                }}
              >
                &#9932;
                <div style={{ fontSize: "10px" }}>CLOSE</div>
              </span>

              <div className="nav-bar">
                <Link
                  to={"/"}
                  style={{ textDecoration: "none" }}
                  className="box-nav"
                >
                  Sign In
                </Link>
                <Link
                  to={"/Register"}
                  style={{ textDecoration: "none" }}
                  className="box-nav"
                >
                  Register
                </Link>


                <div
                  className="box-nav"
                  style={{ cursor: "pointer" }}
                  onClick={() => this.setState({ dd: !this.state.dd })}
                >
                  Dropdown
                  <i className="fa fa-caret-down" />
                </div>
                <div className={!this.state.dd ? "dropdown-container" : null}>
                  <Link
                    to={"/Home"}
                    style={{ textDecoration: "none" }}
                    className="box-nav-dd"
                  >
                    Home
                  </Link>
                  <Link
                    to={"/Group"}
                    style={{ textDecoration: "none" }}
                    className="box-nav-dd"
                  >
                    Group
                  </Link>
                </div>



                <hr style={{width:"90%"}}/>
                <Link
                  to={"/"}
                  style={{ textDecoration: "none" }}
                  className="box-nav"
                >
                  Log Out
                </Link>
              </div>
            </>
          ) : (
            <span
              className="span-bar"
              onClick={() => {
                this.setState({ BarNavigate: !this.state.BarNavigate });
              }}
            >
              &#9776;
              <div style={{ fontSize: "10px" }}>MENU</div>
            </span>
          )}
        </div>
      </div>
    );
  }
}

export default NavigationBar;
